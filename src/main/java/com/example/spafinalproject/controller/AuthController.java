package com.example.spafinalproject.controller;

import com.example.spafinalproject.model.Auth;
import com.example.spafinalproject.model.AuthDto;
import com.example.spafinalproject.model.RegisterDto;
import com.example.spafinalproject.model.Users;
import com.example.spafinalproject.repository.UsersRepository;
import com.example.spafinalproject.service.AuthService;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private final AuthService authService;


    @PostMapping
    public ResponseEntity<?> authentication(@RequestBody RegisterDto auth, @RequestParam(name = "username") String username) {
        try {
            authService.authenticate(auth, new Users(username));
        }catch (Exception e){
            return ResponseEntity.badRequest().body("User not created");

        }
        return ResponseEntity.ok("User created");
    }

    @GetMapping
    public ResponseEntity<?> authorization(@RequestBody AuthDto authDto) {
        try {
            return ResponseEntity.ok(authService.authorize(authDto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

}
