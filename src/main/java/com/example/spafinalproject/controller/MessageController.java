package com.example.spafinalproject.controller;


import com.example.spafinalproject.model.Auth;
import com.example.spafinalproject.model.Message;
import com.example.spafinalproject.service.AuthService;
import com.example.spafinalproject.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/messages")
@AllArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final AuthService authService;


    @GetMapping
    public ResponseEntity<?> getAllByChatId(@RequestParam(name = "id") Long id, @RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        return ResponseEntity.ok(messageService.findAllByChatId(id, auth.getUserId()));
    }



    @PostMapping
    public ResponseEntity<?> saveMessage(@RequestBody Message message, @RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        try {
            messageService.addMessage(message);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Message added");
    }

    @PutMapping
    public ResponseEntity<?> updateMessage(@RequestBody Message message,@RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        try {
            messageService.updateMessage(message);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Message updated");
    }

    @DeleteMapping
    public ResponseEntity<?> deleteMessage(@RequestParam(name = "id") Long id,@RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("Token is invalid");
        }
        try {
            messageService.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("Message not found or deleted " + id);
        }
        return ResponseEntity.ok("Message successfully Deleted:");
    }

}
