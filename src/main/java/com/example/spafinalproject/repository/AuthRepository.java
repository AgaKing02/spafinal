package com.example.spafinalproject.repository;


import com.example.spafinalproject.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth,Long> {
    Auth getByToken(String token);
    Auth getByLoginAndPassword(String login, String password);
}
