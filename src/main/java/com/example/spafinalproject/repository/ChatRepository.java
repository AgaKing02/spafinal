package com.example.spafinalproject.repository;


import com.example.spafinalproject.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
  Chat getById(Long id);
}
