package com.example.spafinalproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpafinalprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpafinalprojectApplication.class, args);
	}

}
