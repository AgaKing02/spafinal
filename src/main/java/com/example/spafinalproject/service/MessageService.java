package com.example.spafinalproject.service;


import com.example.spafinalproject.model.Message;
import com.example.spafinalproject.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;


    public List<Message> findLastMessagesByChatId(Long chatId, Long userId) {
        return messageRepository.findAllByChatId(chatId, PageRequest.of(1, 10, Sort.by(Sort.Direction.DESC, "created_timestamp")).first());
    }

    public List<Message> findAllByChatId(Long chatId, Long userId) {
        return messageRepository.findAllByChatId(chatId);

    }

    public Message addMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() != null) throw new Exception("message has id");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        return messageRepository.save(message);
    }

    public Message updateMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() == null) throw new Exception("message id is null");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        Optional<Message> messageDB = messageRepository.findById(message.getId());
        if (messageDB.isEmpty()) throw new Exception("message not found");

        String text = message.getText();
        message = messageDB.get();
        Long date = new Date().getTime();
        message.setText(text);
        return messageRepository.save(message);
    }



    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }
}
