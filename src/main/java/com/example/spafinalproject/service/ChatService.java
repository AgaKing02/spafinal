package com.example.spafinalproject.service;


import com.example.spafinalproject.model.Chat;
import com.example.spafinalproject.repository.ChatRepository;
import com.example.spafinalproject.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;


    private final UsersRepository usersRepository;



    public List<Chat> findAll() {
        return chatRepository.findAll();
    }

    public Chat save(Chat message) {
        return chatRepository.save(message);
    }

    public void deleteById(Long id) {
        chatRepository.deleteById(id);
    }
}
