package com.example.spafinalproject.service;


import com.example.spafinalproject.model.Auth;
import com.example.spafinalproject.model.AuthDto;
import com.example.spafinalproject.model.RegisterDto;
import com.example.spafinalproject.model.Users;
import com.example.spafinalproject.repository.AuthRepository;
import com.example.spafinalproject.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UsersRepository usersRepository;

    public Auth getByToken(String token) {
        return authRepository.getByToken(token);
    }

    public void authenticate(RegisterDto auth, Users user) {
        Users newUser = usersRepository.save(user);

        Long time = new Date().getTime();

        auth.setLastLoginTimestamp(time);
        auth.setUserId(newUser.getId());
        saveRegister(auth);
    }

    public Auth save(Auth auth) {
        return authRepository.save(auth);
    }
    public Auth saveRegister(RegisterDto registerDto){
        return authRepository.save(new Auth(registerDto));
    }

    public String authorize(AuthDto authDto) {
        Auth auth = authRepository.getByLoginAndPassword(authDto.getLogin(), authDto.getPassword());
        UUID token = UUID.randomUUID();

        auth.setToken(token.toString());
        return authRepository.save(auth).getToken();
    }
}
