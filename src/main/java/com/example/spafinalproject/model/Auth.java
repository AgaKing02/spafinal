package com.example.spafinalproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "auth")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Auth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;
    private String password;

    @Column(name = "last_login_timestamp")
    private Long lastLoginTimestamp;

    @Column(name = "user_id")
    private Long userId;

    private String token;

    public Auth(RegisterDto registerDto){
        this.userId=registerDto.getUserId();
        this.login= registerDto.getLogin();
        this.password=registerDto.getPassword();
        this.lastLoginTimestamp=registerDto.getLastLoginTimestamp();
        this.token= registerDto.getToken();
    }
}
