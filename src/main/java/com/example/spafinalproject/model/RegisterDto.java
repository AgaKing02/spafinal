package com.example.spafinalproject.model;

import lombok.Data;

@Data
public class RegisterDto {
    private String login;
    private String password;
    private Long lastLoginTimestamp;
    private Long userId;

    private String token;
}
